<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css" />
        <link href="{{ asset('css/light.css') }}" rel="stylesheet">
    </head>

    <body class="fixed-header  mac desktop menu-unpinned js-focus-visible pace-done">

        <!-- React root DOM -->
        <div id="root" style="height:100%;">
        </div>

        <!-- React JS -->
        <!-- <script type="text/javascript"src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDIM1gDgOwhWmvYRhInudBpUfgdsSxVcs&libraries=places,geocoding"></script> -->
        <script src="{{ asset('js/app.js') }}" defer></script>

    </body>
</html>