<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->bigInteger('user_id')->nullable(false)->unsigned();
            $table->string('link_full')->nullable(false);
            $table->string('link_protocol')->nullable(false);
            $table->string('link_domain')->nullable(false);
            $table->string('link_path')->nullable();
        });

        Schema::table('links', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
