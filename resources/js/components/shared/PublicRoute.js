import React from 'react'
import { Route, Redirect } from 'react-router'

const PublicRoute = ({ isLoggedIn, ...props }) => {

    if(isLoggedIn == true) {
        return <Redirect to="/" />;
    } 
    
    if(isLoggedIn == undefined) {
        return null
    }
    
    if(isLoggedIn == false) {
        return <Route {...props}/>
    }
};

export default PublicRoute