<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResource;

class LinkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'link' => $this->link_full,
            'protocol' => $this->link_protocol,
            'domain' => $this->link_domain,
            'path' => $this->link_path,
            'user' => new UserResource($this->user_info)
        ];
    }
}