<?php

use Illuminate\Foundation\LinkChecker;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/
Artisan::command('check-links', function () {
    $this->comment(LinkChecker::run());
})->purpose('Checking all links');
