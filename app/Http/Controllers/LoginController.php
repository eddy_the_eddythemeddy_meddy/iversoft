<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;
use App\Http\Requests\RegisterRequest;
use DB;

class LoginController extends Controller
{
    public function login(Request $request) {

        $login = $request->validate([
            'email' => 'required:string',
            'password' => 'required:string'
        ]);

        //user sent their email 
        Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);

        if(!Auth::check()) {
            return response([
                'status' => 'fail',
                'message' => 'Invalid credentials'
            ]);
        }

        $user = Auth::user();

        $accessToken = Auth::user()
            ->createToken('authToken')
            ->accessToken;
        
        return response([
            'status' => 'success',
            'access_token' => $accessToken 
        ]);
    }

    public function register(RegisterRequest $request)
    {
        $request->persist();
        
        return response(
            [
                'status' => 'success',
                'message' => 'Please log in with your email and password now'
            ]
        
        );
    }

    public function is_login(Request $request)
    {
        $is_login = false;
        $userObj = null;

        if ($request->user('api')) {
            $is_login = true;
            $user = $request->user('api');
            $userObj = new User_Resource($user);
        }
        
        return [
            'is_login' => $is_login,
            'user' => $userObj
        ];
    }

    public function logout(Request $request)
    {
        if (Auth::check()) {
            Auth::user()->tokens->each(function($token, $key) {
                $token->delete();
            });
        }
        return $this->is_login($request);
    }

}
