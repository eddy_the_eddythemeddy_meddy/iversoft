<?php

namespace App\Http\Requests;

use App\Models\Links;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class CreateLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'link' => 'Link'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'values.link' => ['required','newurl']
        ];
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $values = $this->input('values');
        $userDetails = Auth::user();
        $url = parse_url($values['link']);

        $this->merge([
            'link_full' => $values['link'],
            'link_protocol' => $url['scheme'],
            'link_domain' => $url['host'],
            'link_path' => $url['path'] ?? null,
            'user_id' => $userDetails->id
        ]);
    }

    public function persist()
    {
        $values = $this->input("values");
        $Link = new Links;

        $Link->fill(
            $this->only([
                'link_full',
                'link_protocol',
                'link_domain',
                'link_path',
                'user_id'
            ])
        );

        $Link->save();

        return $Link;
    }

}