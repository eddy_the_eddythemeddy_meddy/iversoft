<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'App\Http\Controllers\LoginController@register');
Route::post('login', 'App\Http\Controllers\LoginController@login');

Route::middleware('auth:api')->group(function() {
    Route::post('links/create', 'App\Http\Controllers\LinkController@create_link');
    Route::post('links/{link_id}', 'App\Http\Controllers\LinkController@view_link');
    Route::post('links/update/{link_id}', 'App\Http\Controllers\LinkController@update_link');
    Route::post('links/delete-id/{link_id}', 'App\Http\Controllers\LinkController@delete_link');
    Route::post('links/delete-domain/{link_domain}', 'App\Http\Controllers\LinkController@delete_link_specific_domain');
    Route::get('links', 'App\Http\Controllers\LinkController@get_links_all');
    Route::get('links/mine', 'App\Http\Controllers\LinkController@get_links_member');
});