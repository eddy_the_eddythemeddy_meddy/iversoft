import React from 'react'
import { Route, Redirect } from 'react-router'

const ProtectedRoute = ({ isLoggedIn, ...props }) => {

    if(isLoggedIn == false) {
        return <Redirect to="/login" />;
    } 
    
    if(isLoggedIn == undefined) {
        return null
    }
    
    if(isLoggedIn == true) {
        return <Route {...props}/>
    }
};

export default ProtectedRoute