<?php

namespace App\Http\Requests;

use App\Models\Links;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'link' => 'Link'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'values.link' => ['required','newurl']
        ];
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $id = $this->route('link_id');
        $this->link = Links::findorFail($id);
        
        $values = $this->input('values');
        $url = parse_url($values['link']);

        $this->merge([
            'link_full' => $values['link'],
            'link_protocol' => $url['scheme'],
            'link_domain' => $url['host'],
            'link_path' => $url['path'] ?? null
        ]);
    }

    public function persist()
    {
        $this->link->fill(
            $this->only([
                'link_full',
                'link_protocol',
                'link_domain',
                'link_path',
                'user_id'
            ])
        );

        $this->link->save();

        return $this->link;
    }

}