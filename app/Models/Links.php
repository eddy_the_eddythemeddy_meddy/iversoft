<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Models\User;

class Links extends Model
{
    use HasFactory, Uuids;

    protected $primaryKey = 'id';
    protected $fillable = [
        'link_full',
        'link_protocol',
        'link_domain',
        'link_path',
        'user_id'
    ];
    protected $with = [
        'user_info'
    ];

    public function user_info()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeGetAllLinks($query, $search)
    {
        $links = Links::query();
        if($search) {
            $query = $search['query'] ?? null;
            $sort = $search['sort'] ?? null;
            if($query) {
                $links->where('id', 'LIKE', "%$query%");
                $links->orWhere('link_domain', 'LIKE', "%$query%");
            }
            if($sort) {
                switch($sort) {
                    case 'date-desc':
                        $links->orderBy('created_at', 'DESC');
                        break;
                    case 'date-asc':
                        $links->orderBy('created_at', 'ASC');
                        break;
                    case 'id-desc':
                        $links->orderBy('id', 'DESC');
                        break;
                    case 'id-asc':
                        $links->orderBy('id', 'ASC');
                        break;
                }
            }
        }
        return $links->paginate(10);
    }

    public function scopeGetAllMemberLinks($query)
    {
        $user = Auth::user();
        return Links::where('user_id','=', $user->id)->get();
    }

    public function scopeDeleteLink($query, $link_id)
    {
        $user = Auth::user();
        $link = Links::where('user_id','=', $user->id)
                ->where('id', '=', $link_id);
                
        if(!$link->get()->isEmpty()) {
            $link->delete();
            return response()->json("Deleted link# : $link_id", 200);
        }
        
        return response()->json("Not Links to be Deleted.", 200);
    }

    public function scopeDeleteByDomain($query, $link_id)
    {
        $user = Auth::user();
        $link = Links::where('user_id','=', $user->id)
                ->where('link_domain', '=', $link_id);
                
        if(!$link->get()->isEmpty()) {
            $link->delete();
            return response()->json("Deleted link# : $link_id", 200);
        }
        
        return response()->json("Not Links to be Deleted.", 200);
    }
}
