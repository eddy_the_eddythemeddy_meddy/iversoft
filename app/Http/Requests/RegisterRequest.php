<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email'
        ];
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $values = $this->input('values');
        $this->merge([
            'name' => $values['name'],
            'email' => $values['email'],
            'password' => Hash::make($values['password'])
        ]);
    }

    public function persist()
    {
        $values = $this->input("values");
        $User = new User;

        $User->fill(
            $this->only([
                'name',
                'email',
                'password'
            ])
        );

        $User->save();

        return $User;
    }

}