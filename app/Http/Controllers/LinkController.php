<?php

namespace App\Http\Controllers;

use App\Models\Links;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreateLinkRequest;
use App\Http\Requests\UpdateLinkRequest;
use App\Http\Resources\LinkResource;

class LinkController extends Controller
{
    public function create_link(CreateLinkRequest $request)
    {
        if (Auth::check()) {
            $insert = $request->persist();
            return new LinkResource($insert);
        }
        
        return response()->json("Unauthroized.", 403);
    }

    public function view_link(Request $request)
    {
        $user = Auth::check();
        if ($user) {
            $link = Links::where('id', '=', $request->link_id);
            return LinkResource::collection($link);
        }
        
        return response()->json("Unauthroized.", 403);
    }

    public function get_links_all(Request $request)
    {
        $user = Auth::check();
        if ($user) {
            $params = $request->query->all();
            $links = Links::GetAllLinks($params);
            return LinkResource::collection($links);
        }
        
        return response()->json("Unauthroized.", 403);
    }

    public function get_links_member(Request $request)
    {
        if (Auth::check()) {
            $links = Links::GetAllMemberLinks();
            return LinkResource::collection($links);
        }
        
        return response()->json("Unauthroized.", 403);
    }

    public function update_link(UpdateLinkRequest $request)
    {
        if (Auth::check()) {
            return $request->persist();
        }
        
        return response()->json("Unauthroized.", 403);      
    }

    public function delete_link(Request $request)
    {
        if (Auth::check()) {
            return Links::DeleteLink($request->link_id);
        }
        
        return response()->json("Unauthroized.", 403);      
    }

    public function delete_link_specific_domain(Request $request)
    {
        if (Auth::check()) {
            return Links::DeleteByDomain($request->domain);
        }
        
        return response()->json("Unauthroized.", 403);
    }
}